<?php
/**
*Clase para crear, modificar y eliminar usuarios
*@autor NELSON HERNANDEZ www.nelsonhrj.com.ve
*PRUEBA DE GEOPAGOS
**/

class usuario
{
	private $usuario;
	private $codigousuario;
	private $clave;
	private $edad;
	
	/**
	*METODOS SETTERS Y GETTERS
	**/
	function __construct()
	{
		$this->codigousuario = $this->getcodigousuario();
	}
	function setusuario($usuario)
	{
		$this->usuario = $usuario;
	}
	function getusuario()
	{
		return $this->usuario;
	}
	function setclave($clave)
	{
		$this->clave = $clave;
	}
	function getclave()
	{
		return $this->clave;
	}
	function setedad($edad)
	{
		$this->edad = $edad;
	}
	function getedad()
	{
		return $this->edad;
	}
	function getcodigousuario()
	{
		/*AQUI DEBE IR UN QUERY QUE DEVUELVE EL CODIGOUSUARIO SEA QUE LO TRAIGA DE SESION O LO BUSCA EN LA TABLA POR EL NOMBRE DE USUARIO*/
		return $this->usuario;
	}
	/**
	*Metodo de creacion de usuario
	*@return boolean
	**/
	function crear_usuario()
	{
		if (empty($this->usuario))
		{
			//Debe ingresar un usuario válido.
			return false;
		}
		else if ($this->clave<18)
		{
			//La edad debe ser mayor a 18.
			return false;
		}
		else
		{
			/*Hacemos el insert en la tabla USUARIOS asumiendo conexion. Definimos como el id CODIGOUSUARIO un campo llave AUTOINCREMENT o una secuencia segun sea el caso*/
			/* INSERT INTO USUARIOS (USUARIO, CLAVE, EDAD) VALUES ($this->$usuario, $this->$clave, $this->$edad)*/
			//El usuario ha sido registrado con éxito.
			return true;
		}
	}
	/**
	*Metodo de modificacion de usuario
	*@return boolean
	**/
	function modificar_usuario()
	{
		if (empty($this->usuario))
		{
			//Debe ingresar un usuario válido.
			return false;
		}
		else if ($this->edad<18)
		{
			//La edad debe ser mayor a 18.
			return false;
		}
		else
		{
			/*Hacemos el update en la tabla USUARIOS
			/* UPDATE USUARIOS SET USUARIO = $this->$usuario, CLAVE = $this->$clave, EDAD = $edad WHERE CODIGOUSUARIO = $this->$idusuario*/
			//El usuario ha sido modificado con éxito.
			return true;
		}
	}
	/**
	*Metodo de eliminacion de usuario
	*@return boolean
	**/
	function eliminar_usuario()
	{
		/*Hacemos el delete en la tabla USUARIOS
		/*DELETE FROM USUARIOS WHERE codigousuario = $this->$codigousuario*/
		return true;
	}
}
?>