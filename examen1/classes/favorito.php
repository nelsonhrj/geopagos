<?php
/**
*Clase para crear, modificar y eliminar usuarios favoritos
*@autor NELSON HERNANDEZ www.nelsonhrj.com.ve
*PRUEBA DE GEOPAGOS
**/

class favorito
{
	private $favoritos;

	/**
	*Metodo para consultar los favoritos de un usuario
	*@return boolean
	**/
	function getfavoritos($codigousuario)
	{
		/*Hacemos un select en la tabla FAVORITOS y retormanos un array con los usuarios favoritos*/
		/*SELECT CODIGOUSUARIOFAVORITO FROM FAVORITOS WHERE CODIGOUSUARIO = $codigousuario*/
		$this->favoritos = $favoritos;
		return $this->favoritos;
	}
	/**
	*Metodo para creacion de usuario favorito
	*@return boolean
	**/
	function crear_usuario_favorito($codigousuario, $codigousuariofavorito)
	{
		/*Hacemos el insert en la tabla FAVORITOS con el CODIGOUSUARIO de ambos usuarios*/
		/*INSERT INTO FAVORITOS (CODIGOUSUARIO, CODIGOUSUARIOFAVORITO) values ($codigousuario, $codigousuariofavorito)*/
		return true;
	}
	/**
	*Metodo para modificacion de usuario favorito
	*@return boolean
	**/
	function modificar_usuario_favorito($codigousuario, $codigousuariofavorito, $codigousuariofavorito2)
	{
		/*Hacemos el update en la tabla FAVORITOS*/
		/*UPDATE FAVORITOS SET CODIGOUSUARIOFAVORITO =  $codigousuariofavorito2 WHERE CODIGOUSUARIO = $codigousuario AND CODIGOUSUARIOFAVORITO = $codigousuariofavorito*/
		return true;
	}
	/**
	*Metodo para eliminacion de usuario favorito
	*@return boolean
	**/
	function eliminar_usuario_favorito($codigousuario)
	{
		/*Hacemos el delete en la tabla FAVORITOS
		/*DELETE FROM FAVORITOS WHERE CODIGOUSUARIO = $codigousuario*/
		return true;
	}
}
?>