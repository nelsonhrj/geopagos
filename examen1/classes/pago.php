<?php
/**
*Clase para crear, modificar y eliminar pagos
*@autor NELSON HERNANDEZ www.nelsonhrj.com.ve
*PRUEBA DE GEOPAGOS
**/

class pago
{
	private $codigousuariopago;
	private $importe;
	private $fecha;
	
	/**
	*METODOS SETTERS Y GETTERS
	**/
	function setcodigousuariopago($codigousuariopago)
	{
		$this->codigousuariopago = $codigousuariopago;
	}
	function getcodigousuariopago()
	{
		return $this->codigousuariopago;
	}
	function setimporte($importe)
	{
		$this->importe = $importe;
	}
	function getimporte()
	{
		return $this->importe;
	}
	function setfecha($fecha)
	{
		$this->fecha = $fecha;
	}
	function getfecha()
	{
		return $this->fecha;
	}
	function getcodigopago()
	{
		/*AQUI DEBE IR UN QUERY QUE DEVUELVE EL CODIGOPAGO REGISTRADO DE UN PAGO YA SEA UN ID AUTOINCREMENT O UNA SECUENCIA*/
		return $codigopago = 1;
	}
	/**
	*Metodo para creacion de pago
	*@return boolean
	**/
	function crear_pago_usuario()
	{
		$fechaactual = date('d/m/Y');
		if (($this->importe==0) or (empty($this->importe)))
		{
			//El importe a pagar debe ser mayor a cero.
			return false;
		}
		else if ($this->fecha<$fechaactual)
		{
			//La fecha ingresada no pude ser anterior a la fecha actual.
			return false;
		}
		else
		{
			/*Hacemos el insert en la tabla PAGOS. Definimos como el id CODIGOPAGO un campo llave AUTOINCREMENT o una secuencia segun sea el caso*/
			/*INSERT INTO PAGOS (IMPORTE, FECHA) values ($this->importe, $this->fecha)*/
			/*Hacemos el insert en la tabla USUARIOSPAGOS consultando el id generado para el pago insertado
			/*INSERT INTO USUARIOPAGOS (CODIGOPAGO, CODIGOUSUARIO) values (mysqli_insert_id(), $this->codigousuariopago)*/
			//El pago ha sido generado exitosamente.
			return true;
		}
	}
	/**
	*Metodo para modificacion de pago
	*@return boolean
	**/
	function modificar_pago_usuario()
	{
		$codigopago = $this->getcodigopago();
		/*Hacemos el update en la tabla PAGOS
		/*UPDATE PAGOS SET IMPORTE =  $this->importe, FECHA = $this->fecha WHERE CODIGOPAGO = $codigopago*/
		//El pago ha sido modificado.
		return true;
	}
	/**
	*Metodo para eliminacion de pago
	*@return boolean
	**/
	function eliminar_pago_usuario()
	{
		/*Hacemos el delete en la tabla PAGOS
		/*DELETE FROM PAGOS WHERE CODIGOPAGO IN (SELECT CODIGOPAGO FROM PAGOS WHERE CODIGOUSUARIO = $this->codigousuariopago)*/
		/*Hacemos el delete en la tabla USUARIOSPAGOS
		/*DELETE FROM USUARIOSPAGOS WHERE CODIGOUSUARIO = $this->codigousuariopago*/
		//Los pagos han sido eliminado
		return true;
	}
}
?>