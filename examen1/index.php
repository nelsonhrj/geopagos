<?php
/**
*Ejercicio de ingreso de GEOPAGOS
*@autor NELSON HERNANDEZ www.nelsonhrj.com.ve
**/

include 'classes/usuario.php';
include 'classes/pago.php';
include 'classes/favorito.php';

/*CREACION Y MODIFICACION DE USUARIO*/
$main = new usuario;
$main->setusuario('JhonDoe');
$main->setclave('123456');
$main->setedad('19');

	if($main->crear_usuario())
	{
		echo "Usuario ".$main->getusuario()." creado.<br>";
	}
	
$main->setusuario('JhonDoe2');
$main->setclave('1234567');
$main->setedad('20');

	if($main->modificar_usuario())
	{
		echo "Usuario ".$main->getusuario()." modificado.<br>";
	}


/*CREACION Y MODIFICACION DE USUARIOS FAVORITOS*/

$favorito = new favorito;

/*INDICAMOS UN CODIGOUSUARIO PARA AGREGAR A FAVORITO
PARA ESTE EJEMPLO ASIGNAMOS UN CODIGO DE PRUEBA QUE SE SUPONE YA ES UN USUARIO VALIDO EXISTENTE*/

$codigousuarioprueba = 2;

	if($arrayfavoritos = $favorito->crear_usuario_favorito($main->getcodigousuario(),$codigousuarioprueba))
	{
		echo "Usuario favorito asignado.<br>";
	}

/*INDICAMOS LOS CODIGOUSUARIO: EL QUE YA EXISTE PARA ESTE USUARIO Y EL QUE LO REEMPLAZARA.
PARA ESTE EJEMPLO ASIGNAMOS DOS CODIGOS DE PRUEBA QUE SE SUPONE YA SON USUARIOS VALIDOS EXISTENTES*/
$codigousuarioprueba = 2;
$codigousuarioprueba2 = 3;

	if($favorito->modificar_usuario_favorito($main->getcodigousuario(), $codigousuarioprueba, $codigousuarioprueba2))
	{
		echo "Usuario favorito modificado.<br>";
	}

/*CREACION Y MODIFICACION DE PAGOS*/

$pago = new pago;
$pago->setcodigousuariopago($main->getcodigousuario());
$pago->setimporte('100');
$pago->setfecha(date('d/m/Y'));

	if ($pago->crear_pago_usuario())
	{
		echo "El pago ha sido realizado ".$pago->getimporte()."$ ".$pago->getfecha().".<br>";
	}

$pago->setimporte('200');
$pago->setfecha(date('d/m/Y'));

	if ($pago->modificar_pago_usuario())
	{
		echo "El pago ha sido modificado ".$pago->getimporte()."$ ".$pago->getfecha().".<br>";
	}

/*ELIMINACION EN CASCADA PARA QUE NO QUEDEN REGISTROS MUERTOS*/

	if ($pago->eliminar_pago_usuario())
	{
		echo "Los pagos del usuario se han eliminado.<br>";
	}

	if ($favorito->eliminar_usuario_favorito($main->getcodigousuario()))
	{
		echo "Los usuarios favoritos del usuario se han eliminado.<br>";
	}
	
	if ($main->eliminar_usuario())
	{
		echo "El usuario se ha eliminado.<br>";
	}
?>