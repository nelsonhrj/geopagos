<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'FiguraGeometrica.php';
/**
 * Controlador triangulo que calcula las pripiedades del triangulo
 * @autor Nelson Hernandez
 * se setean valores aleatorios en el constructor a modo de prueba
 */

class Triangulo extends FiguraGeometrica {

	private $base;
	private $altura;
	
	function __construct()
	{
        parent::__construct();
		$this->setbase(rand(1,10));
		$this->setaltura(rand(1,10));
	}
	function setbase($base)
	{
		$this->base = $base;
	}
	function setaltura($altura)
	{
		$this->altura = $altura;
	}
	function index()
	{
		$this->superficie = (($this->base*$this->altura)/2);
		$this->base = $this->base;
		$this->altura = $this->altura;
		$this->diametro = 'No Aplica';
		$this->tipofigura = 'triangulo';
		
		$array = array('superficie'=>$this->superficie,
						'base'=>$this->base,
						'altura'=>$this->altura,
						'diametro'=>$this->diametro,
						'tipofigura'=>$this->tipofigura);

		$this->session->set_flashdata('valores',$array);
		redirect('/FiguraGeometrica/figura_geometrica');
	}
}
