<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'FiguraGeometrica.php';
/**
 * Controlador cuadrado que calcula las propiedades de cuadrado
 * @autor Nelson Hernandez
 * se setean valores aleatorios en el constructor a modo de prueba
 */

class Cuadrado extends FiguraGeometrica {

	private $lado;
	
	function __construct()
	{
        parent::__construct();
		$this->setlado(rand(1,10));
	}
	function setlado($lado)
	{
		$this->lado = $lado;
	}
	function index()
	{
		$this->superficie = ($this->lado*$this->lado);
		$this->base = $this->lado;
		$this->altura = $this->lado;
		$this->diametro = 'No Aplica';
		$this->tipofigura = 'cuadrado';
		
		$array = array('superficie'=>$this->superficie,
						'base'=>$this->base,
						'altura'=>$this->altura,
						'diametro'=>$this->diametro,
						'tipofigura'=>$this->tipofigura);

		$this->session->set_flashdata('valores',$array);
		redirect('/FiguraGeometrica/figura_geometrica');
	}
}