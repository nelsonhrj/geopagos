<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'FiguraGeometrica.php';
/**
 * Controlador circulo que calcula las pripiedades de circulo
 * @autor Nelson Hernandez
 **/

class Circulo extends FiguraGeometrica {

	static $pi = 3.14;
	private $radio;

	function __construct()
	{
        parent::__construct();
		$this->setradio(rand(1,10));
	}
	function setradio($radio)
	{
		$this->radio = $radio;
	}
	function index()
	{
		$this->superficie = (self::$pi*(pow($this->radio,2)));
		$this->base = 'No Aplica';
		$this->altura = 'No Aplica';
		$this->diametro = ($this->radio*2);
		$this->tipofigura = 'circulo';
		
		$array = array('superficie'=>$this->superficie,
						'base'=>$this->base,
						'altura'=>$this->altura,
						'diametro'=>$this->diametro,
						'tipofigura'=>$this->tipofigura);

		$this->session->set_flashdata('valores',$array);
		redirect('/FiguraGeometrica/figura_geometrica');
	}
}
