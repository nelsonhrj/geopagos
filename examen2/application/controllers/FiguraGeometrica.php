<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controlador principal que asigna los atributos comunes a las figuras geométricas
 * @autor Nelson Hernandez
 **/

class FiguraGeometrica extends CI_Controller {
	
	private $superficie;
	private $base;
	private $altura;
	private $diametro;
	private $tipofigura;
	
	function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('session');
    }
	function index()
	{
		$this->load->view('index');
	}
	function figura_geometrica()
	{
		
		$valores = $this->session->flashdata('valores');

		$this->load->view('index');
	}
}
