<?php
/**Examen de admision de GEOPAGOS
* @autor Nelson Hernandez
**/
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Exámen de ingreso Geopagos</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Figura Geométrica</h1>

	<div id="body">
	<form action="<?php echo base_url();?>index.php/cuadrado">
		<label for="tipo">Seleccione el tipo de figura geometrica a calcular:</label>
		<select id="tipo">
        <option value="<?php echo base_url();?>index.php/cuadrado">Cuadrado</option>
        <option value="<?php echo base_url();?>index.php/triangulo">Triangulo</option>
        <option value="<?php echo base_url();?>index.php/circulo">Circulo</option>
        </select><br><br>
        <input type="submit" value="Calcular Valores"><br><br>
	</form>
    <?php if ($this->session->flashdata('valores'))
		{
			$valores = $this->session->flashdata('valores');

			echo "Figura: ".$valores['tipofigura'].'<br>';
			echo "Superficie: ".$valores['superficie'].'<br>';
			echo "Base: ".$valores['base'].'<br>';
			echo "Altura: ".$valores['altura'].'<br>';
			echo "Diametro: ".$valores['diametro'].'<br>';
		}
	?>
	</div>
    <script>
	document.getElementById("tipo").onchange = function() {
	document.forms[0].action = this.value;
	}
	</script>
</div>
</body>
</html>